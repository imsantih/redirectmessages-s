package me.imsanti.dev;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;
import me.imsanti.dev.configs.ConfigFile2;
import me.imsanti.dev.configs.ConfigManager2;
import me.imsanti.dev.listeners.CommandListener;
import me.imsanti.dev.managers.BungeeManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.messaging.PluginMessageListener;
import org.jetbrains.annotations.NotNull;

public final class RedirectMessages extends JavaPlugin implements PluginMessageListener {

    private final BungeeManager bungeeManager = new BungeeManager();
    private final ConfigManager2 configManager = new ConfigManager2(this);
    private final ConfigFile2 configFile = new ConfigFile2(configManager, this);

    private static RedirectMessages instance;
    @Override
    public void onEnable() {
        instance = this;
        createConfig();
        this.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
        registerEvents();
        // Plugin startup logic

    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }

    public BungeeManager getBungeeManager() {
        return bungeeManager;
    }

    private void registerEvents() {
        Bukkit.getPluginManager().registerEvents(new CommandListener(this), this);
    }

    @Override
    public void onPluginMessageReceived(@NotNull String channel, @NotNull Player player, @NotNull byte[] message) {
        if(!channel.equals("BungeeCord")) return;
        ByteArrayDataInput in = ByteStreams.newDataInput(message);
    }



    public static RedirectMessages getInstance() {
        return instance;
    }

    private void createConfig() {
        if(configFile.createConfigFile()) {
            Bukkit.getConsoleSender().sendMessage("[Config-Manager] Created config.yml");
        }else {
            Bukkit.getConsoleSender().sendMessage("[Config-Manager] Loaded config.yml");

        }
    }
}
