package me.imsanti.dev.configs;

import me.imsanti.dev.RedirectMessages;
import org.bukkit.configuration.file.FileConfiguration;

import java.io.File;

public class ConfigFile2 {

    private final RedirectMessages redirectMessages;

    public ConfigFile2(ConfigManager2 configManager, final RedirectMessages redirectMessages) {
        this.configManager = configManager;
        this.redirectMessages = redirectMessages;
    }

    private final ConfigManager2 configManager;

    public boolean createConfigFile() {
        if(configManager.createConfigFile(String.valueOf(redirectMessages.getDataFolder()), "config")) return true;

        return false;
    }

    public void saveConfigFile(FileConfiguration config) {
        configManager.saveConfigFile(getConfigFile(), config);
    }

    public FileConfiguration getConfig() {
        return configManager.getConfigFromFile(String.valueOf(redirectMessages.getDataFolder()), "config");
    }

    public File getConfigFile() {
        return new File(redirectMessages.getDataFolder(), "config.yml");
    }
}
