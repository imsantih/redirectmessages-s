package me.imsanti.dev.listeners;

import me.imsanti.dev.RedirectMessages;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.server.ServerCommandEvent;

public class CommandListener implements Listener {

    private final RedirectMessages redirectMessages;

    public CommandListener(final RedirectMessages redirectMessages) {
        this.redirectMessages = redirectMessages;
    }
    @EventHandler
    private void handleStop(final ServerCommandEvent event) {
        final String executedCommand = event.getCommand().split(" ", 2)[0].toLowerCase();
        if(event.getCommand().contains("creative")) return;
        if(executedCommand.equals("stop") || executedCommand.equals("bukkit:stop") || executedCommand.equals("minecraft:stop") || executedCommand.equals("bukkit:restart") || executedCommand.equals("restart") || executedCommand.equals("minecraft:restart")) {
            redirectMessages.getBungeeManager().connectAll(redirectMessages.getConfig().getString("kick-server"));

        }
    }

    @EventHandler
    private void handlePlayerStop(final PlayerCommandPreprocessEvent event) {
        if(event.getMessage().contains("creative")) return;
        final String executedCommand = event.getMessage().substring(1).split(" ", 2)[0].toLowerCase();
        if(executedCommand.equals("stop") || executedCommand.equals("bukkit:stop") || executedCommand.equals("minecraft:stop") || executedCommand.equals("bukkit:restart") || executedCommand.equals("restart") || executedCommand.equals("minecraft:restart")) {
            if(!event.getPlayer().hasPermission("minecraft.command.stop") && !event.getPlayer().hasPermission("bukkit.command.stop") && !event.getPlayer().hasPermission("minecraft.command.restart")) return;

            redirectMessages.getBungeeManager().connectAll(redirectMessages.getConfig().getString("kick-server"));

        }
    }
}
