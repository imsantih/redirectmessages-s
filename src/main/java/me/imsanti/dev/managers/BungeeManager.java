package me.imsanti.dev.managers;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import me.imsanti.dev.RedirectMessages;
import me.imsanti.dev.utils.ColorUtils;
import org.bukkit.Bukkit;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class BungeeManager {

    private final Map<UUID, String> recentServer = new HashMap<>();

    public void connectAll(final String serverName) {
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF("Connect");
        out.writeUTF(serverName);
        Bukkit.getOnlinePlayers().forEach(player -> {
            player.sendPluginMessage(RedirectMessages.getInstance(), "BungeeCord", out.toByteArray());
            Bukkit.getConsoleSender().sendMessage("Player " + player.getName() + " teleported to other server.");
            player.sendMessage(ColorUtils.color(RedirectMessages.getInstance().getConfig().getString("kick-message")));
        });

        try {
            Thread.sleep(2000L);
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        }
    }

    public Map<UUID, String> getRecentServer() {
        return recentServer;
    }
}
